import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {RequestService} from "../../providers/request.service";
import {User} from "../../models/user.model";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {CreditCard} from "../../models/creditCard.model";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../providers/user.service";

@Component({
  selector: 'app-atm-manage-clients',
  templateUrl: './atm-manage-clients.component.html',
  styleUrls: ['./atm-manage-clients.component.css']
})
export class AtmManageClientsComponent implements OnInit, AfterViewInit {
  public isFetching: boolean = false;
  public loadedClients: User[] = [];
  public dataSource: MatTableDataSource<User> = new MatTableDataSource();
  public displayedColumns: string[] = ['position', 'name', 'email', 'edit'];
  public editMode: boolean = false;
  public selectedClient: User = {} as User;
  public isCardSelected: boolean = false;
  public selectedCard: CreditCard = {} as CreditCard;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private requestService: RequestService,
    private router: Router,
    private route: ActivatedRoute,
    public userService: UserService
  ) { }


  ngOnInit(): void {
    this.isFetching = true;
    this.requestService.fetchClients()
      .subscribe(clients => {
        this.loadedClients = clients;
        this.dataSource = new MatTableDataSource<User>(this.loadedClients);
        this.isFetching = false;
      });
  }

  ngAfterViewInit(): void {
    this.requestService.fetchClients()
      .subscribe(clients => {
        this.loadedClients = clients;
        this.dataSource.paginator = this.paginator;
      });
  }

  public displayClientOnTap(user: User): void {
    this.selectedClient = user;
    this.resetEditMode();
    this.editMode = true;
  }

  public displayCardInfoOnTap(card: CreditCard): void {
    this.isCardSelected = true;
    this.selectedCard = card;
  }

  public resetEditMode(): void {
    this.isCardSelected = false;
    this.selectedCard = {} as CreditCard;
  }

  public enableEditClient(): void {
    this.userService.editableClient = this.selectedClient;
    this.router.navigate(['../edit'], {relativeTo: this.route});
  }
}
