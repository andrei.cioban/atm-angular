import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmManageClientsComponent } from './atm-manage-clients.component';

describe('AtmManageClientsComponent', () => {
  let component: AtmManageClientsComponent;
  let fixture: ComponentFixture<AtmManageClientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtmManageClientsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AtmManageClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
