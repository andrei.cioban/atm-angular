import { Component, OnInit } from '@angular/core';
import {UserService} from "../../providers/user.service";
import {RequestService} from "../../providers/request.service";
import {FormControl, Validators} from "@angular/forms";
import {BanknoteInventory} from "../../models/banknoteInventory.model";
import {UserSummary} from "../../models/userSummary.model";
import {HttpErrorResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
  public amount: FormControl = new FormControl(0, [Validators.required, Validators.pattern(/^[1-9][0-9]*$/)]);
  public isFetching: boolean = false;
  public transactionCompleted: boolean = false;
  public loadedCashAmounts: BanknoteInventory[] = [];
  public currentUser: UserSummary;
  public selectedCardIndex: number;
  public isWithdrawFormLoaded: boolean = false;

  constructor(private userService: UserService,
              private requestService: RequestService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.currentUser = this.userService.currentUser;
    if (this.currentUser.role === 'admin') {
      this.isWithdrawFormLoaded = true;
      this.selectedCardIndex = 0;
    }
  }

  public showWithdrawFormOnTap(creditCardIndex: number): void {
    this.selectedCardIndex = creditCardIndex;
    this.isWithdrawFormLoaded = true;
  }

  public onWithdraw(): void {
    this.isWithdrawFormLoaded = false;
    this.isFetching = true;
    this.requestService
      .withdraw(this.amount.value, this.currentUser.creditCards[this.selectedCardIndex].id)
      .subscribe({
        next: (cashAmounts) => {
          this.isFetching = false;
          this.transactionCompleted = true;
          this.loadedCashAmounts = cashAmounts;
        },
        error: (error: HttpErrorResponse) => {
          if (error.status === 400) {
            this.isFetching = false;
            this.isWithdrawFormLoaded = true;
            this.amount.reset();
            this.openSnackBar(error.error, "OK");
          }
        }
      });
  }

  public onResetWithdrawal(): void {
    this.loadedCashAmounts = [];
    this.transactionCompleted = false;
    this.amount.reset();
    this.isWithdrawFormLoaded = true;
  }

  public openSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action,{
      duration: 3000,
      panelClass: ['red-snackbar']
    });
  }
}
