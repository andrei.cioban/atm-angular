import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RequestService} from "../../providers/request.service";
import {UserService} from "../../providers/user.service";
import {UserSummary} from "../../models/userSummary.model";
import {HttpErrorResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-check-balance',
  templateUrl: './check-balance.component.html',
  styleUrls: ['./check-balance.component.css']
})
export class CheckBalanceComponent implements OnInit {
  public balance: number;
  public isFetching: boolean = false;
  public currentUser: UserSummary;
  public isBalanceLoaded: boolean = false;

  constructor(private router: Router,
              private requestService: RequestService,
              private route: ActivatedRoute,
              private userService: UserService,
              private snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.currentUser = this.userService.currentUser;

    if (this.userService.currentUser.role === 'admin') {
      this.isBalanceLoaded = true;
      this.isFetching = true;

      this.requestService.fetchAtmBalance()
        .subscribe((balance: number) => {
          this.isFetching = false;
          this.balance = balance;
        });
    }
  }

  public showBalanceOnTap(creditCardId: number): void {
    this.isFetching = true;

    this.requestService.fetchClientBalance(creditCardId)
      .subscribe({
        next: (balance: number) => {
          this.isBalanceLoaded = true;
          this.isFetching = false;
          this.balance = balance;
        },
        error: (error: HttpErrorResponse) => {
          this.isBalanceLoaded = false;
          this.isFetching = false;
          this.openSnackBar(error.error, "OK");
        }
      });
  }

  public goBackOnTap(): void {
    this.router.navigate(["../"], {relativeTo:this.route});
  }

  public openSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action,{
      duration: 3000,
      panelClass: ['red-snackbar']
    });
  }
}
