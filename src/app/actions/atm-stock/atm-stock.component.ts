import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {RequestService} from "../../providers/request.service";
import {BanknoteInventory} from "../../models/banknoteInventory.model";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmDialogComponent} from "../../dialogs/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'app-atm-stock',
  templateUrl: './atm-stock.component.html',
  styleUrls: ['./atm-stock.component.css']
})
export class AtmStockComponent implements OnInit {
  public stockForm: FormGroup;
  public isFetching: boolean = false;
  public editMode: boolean = false;
  public stock: BanknoteInventory[] = [];
  public title: string = "SAVE";

  constructor(private requestService: RequestService,
              private dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.isFetching = true;
    this.requestService.fetchStock()
      .subscribe(
        (stock: BanknoteInventory[]) => {
          this.stock = stock;
          this.isFetching = false;
          this.initForm();
        }
      );
  }

  private initForm(): void {
    let stockInventories = new FormArray<FormGroup>([]);
    for (let inventory of this.stock) {
      stockInventories.push(
        new FormGroup({
          'banknote': new FormControl(inventory.banknote),
          'quantity': new FormControl(inventory.quantity, [Validators.required, Validators.pattern(/^[0-9]+$/)])
        })
      );
    }

    this.stockForm = new FormGroup({
      'inventories': stockInventories
    })
  }

  get stockFormInventories() {
    return this.stockForm.get('inventories') as FormArray;
  }

  public onEdit(): void {
    this.editMode = !this.editMode;
    if (this.editMode) {
      this.title = "APPLY";
    } else {
      this.title = "SAVE";
    }
  }

  public onTap(): void {
    if(this.editMode) {
      this.openDialog();
    } else {
      this.requestService.updateStock(this.stockForm.get('inventories')?.value);
      this.router.navigate(["../"], {relativeTo:this.route});
    }
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.fetchDialogResponse(result);
    });
  }

  public fetchDialogResponse(response: boolean): void {
    if (response) {
      this.editMode = false;
      this.title = "SAVE";
    }
  }
}
