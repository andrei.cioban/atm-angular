import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmStockComponent } from './atm-stock.component';

describe('AtmStockComponent', () => {
  let component: AtmStockComponent;
  let fixture: ComponentFixture<AtmStockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtmStockComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AtmStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
