import { Component, OnInit } from '@angular/core';
import {UserService} from "../../providers/user.service";
import {UserSummary} from "../../models/userSummary.model";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RequestService} from "../../providers/request.service";

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {
  public currentUser: UserSummary;
  public isDepositFormLoaded: boolean = false;
  public banknotes: String[] = [];
  public depositForm: FormGroup;
  public isFetching: boolean = false;
  public selectedCardIndex: number;

  constructor(private userService: UserService,
              private requestService: RequestService,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.currentUser = this.userService.currentUser;
  }

  public showDepositFormOnTap(cardIndex: number): void {
    this.isFetching = true;
    this.isDepositFormLoaded = false;
    this.selectedCardIndex = cardIndex;
    this.requestService.fetchBanknotes()
      .subscribe((banknotes: String[]) => {
        this.isFetching = false;
        this.isDepositFormLoaded = true;
        this.banknotes = banknotes;
        this.initForm();
      })
  }

  public initForm(): void {
    this.depositForm = this.formBuilder
      .group({
        banknoteInventories: this.formBuilder.array<FormBuilder>([])
      });

    for (let banknote of this.banknotes) {
      this.banknoteInventories
        .push(this.formBuilder.group({
            banknote: [banknote],
            quantity: [0, [Validators.required, Validators.pattern(/^[0-9]+$/)]],
        }));
    }
  }

  get banknoteInventories() {
    return this.depositForm.controls["banknoteInventories"] as FormArray;
  }

  public onSubmit(): void {
    this.requestService
      .depositMoney(
        this.depositForm.get('banknoteInventories')?.value,
        this.currentUser.creditCards[this.selectedCardIndex].id);
    this.depositForm.reset();
    this.isDepositFormLoaded = false;
  }

}
