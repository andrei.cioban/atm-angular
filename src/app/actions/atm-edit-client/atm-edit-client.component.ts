import { Component, OnInit } from '@angular/core';
import {UserService} from "../../providers/user.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {RequestService} from "../../providers/request.service";

@Component({
  selector: 'app-atm-edit-client',
  templateUrl: './atm-edit-client.component.html',
  styleUrls: ['./atm-edit-client.component.css']
})
export class AtmEditClientComponent implements OnInit {
  public creditCardForm: FormGroup;

  constructor(private userService: UserService,
              private router: Router,
              private requestService: RequestService) { }

  ngOnInit(): void {
    this.initForm();
  }

  public initForm(): void {
    this.creditCardForm = new FormGroup({
      'cardNumber': new FormControl('', [Validators.required]),
      'bankName': new FormControl('', [Validators.required]),
      'cvv': new FormControl('', [Validators.required, Validators.pattern(/^[1-9][0-9]{2}$/)]),
      'pin': new FormControl('', [Validators.required, Validators.pattern(/^[1-9][0-9]{3}$/)]),
      'account': new FormGroup({
        'iban': new FormControl('', [Validators.required]),
        'balance': new FormControl('', [Validators.required, Validators.pattern(/^[0-9]+$/)]),
      }),
      'user': new FormGroup({
        'id': new FormControl(this.userService.editableClient.id)
      })
    });
  }

  public onConfirm(): void {
    const creditCard: CreditCardTemplate = {
      cardNumber: this.creditCardForm.get('cardNumber')?.value,
      bankName: this.creditCardForm.get('bankName')?.value,
      cvv: this.creditCardForm.get('cvv')?.value,
      pin: this.creditCardForm.get('pin')?.value,
      user: {
        id: this.creditCardForm.get('user')?.get('id')?.value
      },
      account: {
        iban: this.creditCardForm.get('account')?.get('iban')?.value,
        balance: this.creditCardForm.get('account')?.get('balance')?.value
      }
    };

    this.requestService.addCreditCard(creditCard);
    this.router.navigate(['/admin/manage']);
  }
}
