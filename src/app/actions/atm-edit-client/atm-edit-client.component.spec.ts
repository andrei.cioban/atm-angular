import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmEditClientComponent } from './atm-edit-client.component';

describe('AtmEditClientComponent', () => {
  let component: AtmEditClientComponent;
  let fixture: ComponentFixture<AtmEditClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtmEditClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AtmEditClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
