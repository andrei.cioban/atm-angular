import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {BanknoteInventory} from "../models/banknoteInventory.model";
import {map, Observable} from "rxjs";
import {User} from "../models/user.model";
import {UserSummary} from "../models/userSummary.model";

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  constructor(private http: HttpClient) { }

  public fetchAtmBalance(): Observable<number> {
    return this.http
      .get<number>("/api/atm/funds/balance");
  }

  public fetchClientBalance(creditCardId: number): Observable<number> {
    return this.http
      .get<number>("/api/atm/balance", {
        params: new HttpParams().set('creditCardId', creditCardId)
      });
  }

  public fetchStock(): Observable<BanknoteInventory[]> {
    return this.http
      .get<BanknoteInventory[]>('/api/atm/funds');
  }

  public updateStock(banknoteInventories: BanknoteInventory[]): void {
    this.http
      .put<BanknoteInventory[]>('/api/atm/funds', banknoteInventories)
      .subscribe(response => {
      })
  }

  public fetchUser(username: string, password: string, role: string): Observable<UserSummary> {
    return this.http
      .get<User>(
        '/api/user',
        {
          params: new HttpParams().set('username', username).set('password', password).set('role', role.toUpperCase())
          },)
      .pipe(map(responseData => {
        let user: UserSummary = new UserSummary();
        user.id = responseData.id;
        user.name = responseData.name;
        user.role = responseData.role.toLowerCase();

        for (let index in responseData.creditCards) {
          const card: CardTuple = {
            id: responseData.creditCards[index].id,
            cardNumber: responseData.creditCards[index].cardNumber
          }

          user.creditCards.push(card);
        }
        return user;
      }));

  }

  public withdraw(amount : number, creditCardId: number): Observable<BanknoteInventory[]> {
    return this.http
      .get<BanknoteInventory[]>(
        '/api/atm/withdraw',
        {
          params: new HttpParams().set('amount', amount).set('creditCardId', creditCardId)
        });
  }

  public fetchUsers(): Observable<User[]> {
    return this.http
      .get<User[]>(
        '/api/users'
      );
  }

  public fetchClients(): Observable<User[]> {
    return this.http
      .get<User[]>(
        'api/users/roles',
        {
          params: new HttpParams().set('role', 'CLIENT')
        }
      );
  }

  public addCreditCard(cardTemplate: CreditCardTemplate): void {
    this.http
      .post<CreditCardTemplate>('/api/creditCards', cardTemplate)
      .subscribe();
  }

  public fetchBanknotes(): Observable<String[]> {
    return this.http
      .get<String[]>('/api/atm/funds/banknotes');
  }

  public depositMoney(banknoteInventories: BanknoteInventory[], creditCardId: number) {
    this.http
      .post<BanknoteInventory[]>('/api/atm/deposit', banknoteInventories, {
        params: new HttpParams().set('creditCardId', creditCardId)
      })
      .subscribe();
  }
}
