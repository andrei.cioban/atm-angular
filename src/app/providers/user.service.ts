import { Injectable } from '@angular/core';
import {UserSummary} from "../models/userSummary.model";
import {User} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public currentUser: UserSummary;
  public isLogged: boolean = false;
  public editableClient: User = <User>{};

  constructor() {
    this.currentUser = new UserSummary();
  }

  public initUser(user: UserSummary): void {
    this.currentUser = user;
    this.isLogged = true;
  }

  public resetUser(): void {
    this.isLogged = false;
    this.currentUser = <UserSummary>{};
    this.editableClient = <User>{};
  }
}
