import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {RequestService} from "../providers/request.service";
import {UserService} from "../providers/user.service";
import {HttpErrorResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public role: string;
  public loginForm: FormGroup;
  public isHiddenPassword: boolean = true;
  public isFetching: boolean = false;

  constructor(private route: ActivatedRoute,
              private requestService: RequestService,
              private router: Router,
              private userService: UserService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      (queryParams: Params) => {
        this.role = queryParams['role'];
      }
    );

    this.initForm();
  }

  public initForm(): void {
    this.loginForm = new FormGroup({
      'username': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required])
    });
  }

  public onSubmit(): void {
    this.isFetching = true;
    this.requestService
      .fetchUser(this.loginForm.value['username'], this.loginForm.value['password'], this.role)
      .subscribe({
        next: (response) => {
          this.userService.initUser(response);
          this.isFetching = false;
          this.router.navigate([this.role]);
        },
        error: (error: HttpErrorResponse) => {
          if (error.status === 404) {
            this.isFetching = false;
            this.openSnackBar("The display name or password was incorrect. Please try again", "OK");
          }
        }
    });
  }

  public openSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action,{
      duration: 3000,
      panelClass: ['red-snackbar']
    });
  }
}
