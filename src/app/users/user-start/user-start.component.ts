import { Component } from '@angular/core';
import {UserService} from "../../providers/user.service";

@Component({
  selector: 'app-user-start',
  templateUrl: './user-start.component.html',
  styleUrls: ['./user-start.component.css']
})
export class UserStartComponent {

  constructor(public userService: UserService) { }

}
