import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import { ErrorPageComponent } from './error-page/error-page.component';
import { AdminComponent } from './users/admin/admin.component';
import { ClientComponent } from './users/client/client.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {HttpClientModule} from "@angular/common/http";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import {MatCardModule} from "@angular/material/card";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { AtmStockComponent } from './actions/atm-stock/atm-stock.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatDialogModule} from "@angular/material/dialog";
import { WithdrawComponent } from './actions/withdraw/withdraw.component';
import { LoginComponent } from './login/login.component';
import { AtmManageClientsComponent } from './actions/atm-manage-clients/atm-manage-clients.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSelectModule} from "@angular/material/select";
import { AtmEditClientComponent } from './actions/atm-edit-client/atm-edit-client.component';
import {MatStepperModule} from "@angular/material/stepper";
import { CheckBalanceComponent } from './actions/check-balance/check-balance.component';
import { UserStartComponent } from './users/user-start/user-start.component';
import { DepositComponent } from './actions/deposit/deposit.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ErrorPageComponent,
    AdminComponent,
    ClientComponent,
    AtmStockComponent,
    WithdrawComponent,
    LoginComponent,
    AtmManageClientsComponent,
    AtmEditClientComponent,
    CheckBalanceComponent,
    UserStartComponent,
    DepositComponent,
    ConfirmDialogComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatDividerModule,
        MatIconModule,
        MatButtonModule,
        MatProgressBarModule,
        HttpClientModule,
        MatSidenavModule,
        MatListModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        MatDialogModule,
        FormsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSelectModule,
        MatStepperModule,
        MatSnackBarModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
