export class Account {
  constructor(
    public id: number,
    public iban: string,
    public balance: number,
    ) {
  }
}
