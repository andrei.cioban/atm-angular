export class UserSummary {
  public id: number;
  public name: string;
  public role: string;
  public creditCards: CardTuple[] = [];

  constructor() {
  }

}
