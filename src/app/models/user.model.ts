import {CreditCard} from "./creditCard.model";

export class User {
  constructor(
    public id: number,
    public name: string,
    public role: string,
    public username: string,
    public password: string,
    public email: string,
    public creditCards: CreditCard[],
  ) {
  }
}
