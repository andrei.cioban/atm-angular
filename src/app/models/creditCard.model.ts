import {Account} from "./account.model";

export class CreditCard {
  constructor(
    public account: Account,
    public bankName: string,
    public cardNumber: string,
    public cvv: number,
    public id: number,
    public pin: number
  ) {
  }
}
