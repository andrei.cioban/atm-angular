export {}

declare global {
  type CreditCardTemplate = {
    cardNumber: string,
    bankName: string,
    cvv: number,
    pin: number,
    user: {
      id: number
    },
    account: {
      iban: string,
      balance: number
    }
  }

  type CardTuple = {
    id: number,
    cardNumber: string
  }
}
