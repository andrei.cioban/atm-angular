import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../providers/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(private router: Router,
              public userService: UserService) { }

  public logoutOnTap(): void {
    this.userService.resetUser();
    this.router.navigate(['/']);
  }
}
