import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {ErrorPageComponent} from "./error-page/error-page.component";
import {AdminComponent} from "./users/admin/admin.component";
import {ClientComponent} from "./users/client/client.component";
import {AtmStockComponent} from "./actions/atm-stock/atm-stock.component";
import {WithdrawComponent} from "./actions/withdraw/withdraw.component";
import {LoginComponent} from "./login/login.component";
import {AtmManageClientsComponent} from "./actions/atm-manage-clients/atm-manage-clients.component";
import {AtmEditClientComponent} from "./actions/atm-edit-client/atm-edit-client.component";
import {CheckBalanceComponent} from "./actions/check-balance/check-balance.component";
import {UserStartComponent} from "./users/user-start/user-start.component";
import {DepositComponent} from "./actions/deposit/deposit.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'admin', component: AdminComponent, children: [
      {path: '', component: UserStartComponent},
      {path: 'balance', component:CheckBalanceComponent},
      {path: 'stock', component:AtmStockComponent},
      {path: 'withdraw', component:WithdrawComponent},
      {path: 'manage', component:AtmManageClientsComponent},
      {path: 'edit', component:AtmEditClientComponent}
    ]},
  {path: 'client', component: ClientComponent, children: [
      {path: '', component: UserStartComponent},
      {path: 'balance', component: CheckBalanceComponent},
      {path: 'deposit', component: DepositComponent},
      {path: 'withdraw', component: WithdrawComponent}
    ]},
  {path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!'}},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
